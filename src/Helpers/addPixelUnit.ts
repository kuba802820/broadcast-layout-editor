import { Component } from '@/schema/db/componentTypes';

const checkIsUndefined = (val: string) =>
    val === 'undefined' ? undefined : val;

export const addPixelUnit = (component: Component) => {
    const propertyToAddPixels = [
        'top',
        'left',
        'width',
        'height',
        'borderRadius'
    ];
    const propertyToIngoreRemovePx = ['text'];
    const arr = Object.entries(component);
    return arr.reduce((acc, [key, val]) => {
        const cleanedFromPxUnitValue = propertyToIngoreRemovePx.includes(key)
            ? val
            : val?.replace(/px/g, '');
        propertyToAddPixels.includes(key)
            ? acc.push([key, checkIsUndefined(`${cleanedFromPxUnitValue}px`)])
            : acc.push([key, checkIsUndefined(`${cleanedFromPxUnitValue}`)]);
        return acc;
    }, [] as [string, string | undefined][]);
};
