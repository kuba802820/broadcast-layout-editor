import useMutationObserver from '@rooks/use-mutation-observer';
import { useRef, useState } from 'react';

export const useHandleHierarchy = () => {
    const [nodes, setNodes] = useState<NodeList[]>();
    const previewRef = useRef<HTMLDivElement>(null);
    useMutationObserver(previewRef, ([mutation]) => {
        if (mutation.addedNodes.length > 0) {
            setNodes((prevNodesList) => [
                ...(prevNodesList ?? []),
                mutation.addedNodes
            ]);
        } else if (mutation.removedNodes.length > 0) {
            setNodes((prevNodesList) =>
                prevNodesList?.filter((val) => {
                    const element = val[0] as HTMLElement;
                    const elementToRemove = mutation
                        .removedNodes[0] as HTMLElement;
                    return element.id !== elementToRemove.id;
                })
            );
        }
    });
    return { previewRef, nodes };
};
