import { Component } from '@/schema/db/componentTypes';
import useMutationObserver from '@rooks/use-mutation-observer';
import { useState } from 'react';

export const usePreviewRenderer = (
    componentsRef: React.RefObject<HTMLDivElement>
) => {
    const [componentsState, setComponents] = useState<Component[]>();
    const getParsedComponents = (components: (string | undefined)[]) => {
        return components.reduce((acc, curr) => {
            if (curr) {
                const parsed: Component = JSON.parse(curr);
                acc.push(parsed);
            }
            return acc;
        }, [] as Component[]);
    };
    useMutationObserver(componentsRef, ([mutation]) => {
        const element = mutation.target as HTMLElement;
        const previewComponent = Object.values(
            Object.assign({}, element.dataset)
        );
        const parsedComponent = getParsedComponents(previewComponent);
        setComponents(parsedComponent);
    });
    return [componentsState];
};
