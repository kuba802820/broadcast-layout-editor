import { Component } from '@/schema/db/componentTypes';
import { deleteFromStorage } from '@rehooks/local-storage';
import { useState } from 'react';
import { useParsedLocalStorage } from './useParsedLocalStorage';
export const useHandleSelection = () => {
    const { parsedValue, setValue } =
        useParsedLocalStorage<Component>('selectionComponent');
    const [loading, setLoading] = useState(false);

    const removeSelectionEffect = () => {
        document.querySelectorAll('.item-selected').forEach((val) => {
            val.classList.remove('item-selected');
        });
    };

    const clearSelection = () => {
        removeSelectionEffect();
        deleteFromStorage('selectionComponent');
    };

    const getDomElement = (id?: string) => {
        if (parsedValue && !id) {
            return document.querySelector(
                `#${parsedValue.id}`
            ) as HTMLDivElement;
        } else {
            if (id) {
                return document.querySelector(`#${id}`) as HTMLDivElement;
            } else {
                return null;
            }
        }
    };
    const selectComponent = (
        item: Component,
        currentTarget: EventTarget & Element
    ) => {
        const SELECTION_DELAY = 200; //Added to fix bug with not updating rich text editor after selection.
        clearSelection();
        setLoading(true);
        setTimeout(() => {
            setValue(JSON.stringify(item));
            removeSelectionEffect();
            currentTarget.classList.add('item-selected');
            setLoading(false);
        }, SELECTION_DELAY);
    };

    return {
        selectComponent,
        parsedComponent: parsedValue,
        clearSelection,
        selectedComponent: setValue,
        getDomElement,
        loading
    };
};
