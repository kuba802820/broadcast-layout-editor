import { Component, ComponentType } from '@/schema/db/componentTypes';
import useLocalStorage from '@rehooks/local-storage';
import { addPixelUnit } from '../../Helpers/addPixelUnit';
import { useManageAnimation } from '../Animation/useManageAnimation';
import { useHandleSelection } from './useHandleSelection';
interface Toolbox {
    createComponent: ({ ...props }: Component) => void;
    deleteComponent: (id: string) => Promise<void>;
    updateComponent: (id: string, component: Component) => void;
    getAllComponentsIds: () => string[] | null;
    selectTool: (toolId: ComponentType) => void;
    selectedTool: ComponentType;
}

export const useToolbox = () => {
    const [selectedTool, setSelectedTool] = useLocalStorage('selectedTool');
    const { clearSelection } = useHandleSelection();
    const { selectComponent, getDomElement } = useHandleSelection();
    const allComponentsDiv = getDomElement('components');
    const { removeAllAnimationsByComponentId } = useManageAnimation();
    const createComponent = async ({ ...props }: Component) => {
        if (allComponentsDiv?.id === 'components') {
            allComponentsDiv.setAttribute(
                `data-${props.id}`,
                JSON.stringify(
                    addPixelUnit(props).reduce(
                        (acc, [k, v]) => ({ ...acc, [k]: v }),
                        {}
                    )
                )
            );
        }
    };
    const deleteComponent = async (id: string) => {
        if (!selectComponent) throw 'Component not selected';
        const DELAY_OF_DELETE_ANIMATIONS = 100;
        if (allComponentsDiv?.id === 'components') {
            clearSelection();
            allComponentsDiv.removeAttribute(`data-${id}`);
            setTimeout(() => {
                removeAllAnimationsByComponentId(id);
            }, DELAY_OF_DELETE_ANIMATIONS);
        }
    };
    const updateComponent = (id: string, component: Component) => {
        if (!selectedTool) throw 'Component not selected';
        if (id && allComponentsDiv?.id === 'components') {
            allComponentsDiv.setAttribute(
                `data-${id}`,
                JSON.stringify(
                    addPixelUnit(component).reduce(
                        (acc, [k, v]) => ({ ...acc, [k]: v }),
                        {}
                    )
                )
            );
        }
    };
    const getAllComponentsIds = () =>
        allComponentsDiv ? Object.keys(allComponentsDiv?.dataset) : null;
    const selectTool = (toolId: ComponentType) => setSelectedTool(toolId);
    return {
        createComponent,
        deleteComponent,
        updateComponent,
        getAllComponentsIds,
        selectTool,
        selectedTool
    } as Toolbox;
};
