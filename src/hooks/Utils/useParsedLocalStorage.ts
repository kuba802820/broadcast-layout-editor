import useLocalStorage from '@rehooks/local-storage';
interface ReturnVal<T> {
    parsedValue: T | undefined;
    setValue: (newValue: string) => void;
}
export const useParsedLocalStorage = <T>(key: string): ReturnVal<T> => {
    const [value, setValue] = useLocalStorage<string>(key);
    try {
        const parsedValue: T | undefined = value
            ? JSON.parse(value)
            : undefined;
        return { parsedValue, setValue };
    } catch (error) {
        return { parsedValue: undefined, setValue };
    }
};
