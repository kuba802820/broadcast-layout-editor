import { Component } from '@/schema/db/componentTypes';
import { useEffect } from 'react';
import { useParsedLocalStorage } from './useParsedLocalStorage';
import { useToolbox } from './useToolbox';

export const useUpdatePreview = () => {
    const { updateComponent } = useToolbox();
    const { parsedValue, setValue } = useParsedLocalStorage<Component>(
        'selectionComponent'
    );

    useEffect(() => {
        const DEBOUNCE_TIMEOUT = 150;
        let isCanceled = false;

        const timeoutHandler = setTimeout(() => {
            if (!isCanceled && parsedValue) {
                updateComponent(parsedValue?.id, parsedValue);
            }
        }, DEBOUNCE_TIMEOUT);

        return () => {
            isCanceled = true;
            clearTimeout(timeoutHandler);
        };
    }, [parsedValue, updateComponent]);
    return {
        parsedValue,
        setValue
    };
};
