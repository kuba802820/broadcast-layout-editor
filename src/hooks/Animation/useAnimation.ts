import { AnimationObject } from '@/components/AnimationSquence';
import { ComponentState } from '@/components/ComponentStatePicker/config';
import useLocalStorage from '@rehooks/local-storage';
import useMutationObserver from '@rooks/use-mutation-observer';
import { useEffect, useState } from 'react';
import { useHandleSelection } from '../Utils/useHandleSelection';

export const useAnimation = (animationRef: React.RefObject<HTMLDivElement>) => {
    const [animState] = useLocalStorage<ComponentState>('selectedAnimState');
    const { getDomElement } = useHandleSelection();
    const [animations, setAnimations] = useState<AnimationObject[] | null>(
        null
    );
    const elementOfAllAnimation = getDomElement('component-animations');

    const setAnimationState = (
        animationsComponent: HTMLElement,
        animState: ComponentState
    ) => {
        const data = JSON.parse(JSON.stringify(animationsComponent.dataset));
        data[animState]
            ? setAnimations(JSON.parse(data[animState]))
            : setAnimations(null);
    };

    useEffect(() => {
        if (elementOfAllAnimation && animState) {
            setAnimationState(elementOfAllAnimation, animState);
        }
    }, [animState, elementOfAllAnimation]);

    useMutationObserver(animationRef, ([mutation]) => {
        if (animState) {
            const animationsDiv = mutation.target as HTMLElement;
            setAnimationState(animationsDiv, animState);
        }
    });
    return { animations, animState };
};
