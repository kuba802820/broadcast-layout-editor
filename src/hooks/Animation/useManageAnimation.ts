import { AnimationObject } from '@/components/AnimationSquence';
import { ComponentState } from '@/components/ComponentStatePicker/config';
import { useHandleSelection } from '../Utils/useHandleSelection';

export const useManageAnimation = () => {
    const { getDomElement } = useHandleSelection();
    const elementOfAllAnimation = getDomElement('component-animations');

    const parseAnimationObject = <T>(state: ComponentState): T | null => {
        if (elementOfAllAnimation) {
            const animData = elementOfAllAnimation.getAttribute(
                `data-${state}`
            );
            return animData ? (JSON.parse(animData) as T) : null;
        } else {
            return null;
        }
    };
    const saveSequenceItem = (state: ComponentState, data: AnimationObject) => {
        if (elementOfAllAnimation) {
            const animData = elementOfAllAnimation.dataset[state];
            if (animData) {
                const animStateData: AnimationObject[] = JSON.parse(animData);
                animStateData.push(data);
                elementOfAllAnimation.setAttribute(
                    `data-${state}`,
                    JSON.stringify(animStateData)
                );
            } else {
                elementOfAllAnimation.setAttribute(
                    `data-${state}`,
                    `[${JSON.stringify(data)}]`
                );
            }
        }
    };

    const moveAnimation = (
        index: number,
        state: ComponentState,
        action: 'up' | 'down'
    ) => {
        const parsedAnim = parseAnimationObject<AnimationObject[]>(state);
        if (elementOfAllAnimation && parsedAnim) {
            const copyOfparsedAnim = parsedAnim;
            switch (action) {
                case 'up':
                    [copyOfparsedAnim[index], copyOfparsedAnim[index - 1]] = [
                        copyOfparsedAnim[index - 1],
                        copyOfparsedAnim[index]
                    ];
                    elementOfAllAnimation.setAttribute(
                        `data-${state}`,
                        JSON.stringify(copyOfparsedAnim)
                    );
                    break;
                case 'down':
                    [copyOfparsedAnim[index], copyOfparsedAnim[index + 1]] = [
                        copyOfparsedAnim[index + 1],
                        copyOfparsedAnim[index]
                    ];
                    elementOfAllAnimation.setAttribute(
                        `data-${state}`,
                        JSON.stringify(copyOfparsedAnim)
                    );
            }
        }
    };

    const removeOneAnimation = (index: number, state: ComponentState) => {
        if (elementOfAllAnimation) {
            const parsedAnim = parseAnimationObject<AnimationObject[]>(state);
            if (parsedAnim) {
                const copyOfparsedAnim = parsedAnim;
                copyOfparsedAnim?.splice(index, 1);
                if (copyOfparsedAnim) {
                    elementOfAllAnimation.setAttribute(
                        `data-${state}`,
                        JSON.stringify(copyOfparsedAnim)
                    );
                }
            }
        }
    };
    const removeAllAnimationsByComponentId = (idComponent: string) => {
        const componentStates: ComponentState[] = [
            'enabledstate',
            'disabledstate',
            'updatestate'
        ];
        for (const state of componentStates) {
            const parsedAnim = parseAnimationObject<AnimationObject[]>(state);
            if (parsedAnim && elementOfAllAnimation && parsedAnim.length > 0) {
                const copyOfparsedAnim = parsedAnim;
                const animAfterRemove = copyOfparsedAnim.filter(
                    (val) => val.seq && val.seq.id !== idComponent
                );
                elementOfAllAnimation.setAttribute(
                    `data-${state}`,
                    JSON.stringify(animAfterRemove)
                );
            }
        }
    };
    const getAnimationByIndex = (index: number, state: ComponentState) => {
        if (elementOfAllAnimation) {
            const parsedAnim = parseAnimationObject<AnimationObject[]>(state);
            return parsedAnim ? parsedAnim[index] : null;
        }
    };
    return {
        saveSequenceItem,
        removeOneAnimation,
        removeAllAnimationsByComponentId,
        getAnimationByIndex,
        moveAnimation
    };
};
