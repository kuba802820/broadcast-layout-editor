import { AnimationObject } from '@/components/AnimationSquence';
import Velocity, { VelocityElements } from 'velocity-animate';
import { useHandleSelection } from '../Utils/useHandleSelection';

export const usePlayAnimation = () => {
    const { getDomElement } = useHandleSelection();
    interface PlayAnimArguments {
        animation: AnimationObject;
        animProperties?: {
            element: VelocityElements;
            params: any;
        };
    }

    const waitForPlayAnim = (time: number) =>
        new Promise((res) => setTimeout(res, time));

    const getAnimProperties = ({ animation }: PlayAnimArguments) => {
        const target = getDomElement(`${animation.seq && animation.seq.id}`);
        if (target) {
            const element = target as VelocityElements;
            const {
                id,
                type,
                imgSrc,
                text,
                isEditabled,
                backgroundColor,
                ...params
            }: any = animation?.seq;
            return {
                element,
                params
            };
        } else {
            return undefined;
        }
    };

    const execAnim = ({ animProperties, animation }: PlayAnimArguments) => {
        if (animProperties) {
            const { element, params } = animProperties;
            if (element && animation) {
                Velocity(element, params, {
                    duration: animation.duration
                });
            }
        }
    };

    const playAnimSeq = (animArray: AnimationObject[]) => {
        animArray.forEach(async (animation, i) => {
            if (!animation.isChangeText) {
                await waitForPlayAnim(animation.delay * i);
                const animProperties = getAnimProperties({ animation });
                execAnim({ animProperties, animation });
            }
        });
    };

    const playAnimation = ({ animation }: PlayAnimArguments) => {
        const animProperties = getAnimProperties({ animation });
        execAnim({ animProperties, animation });
    };
    return {
        playAnimation,
        playAnimSeq
    };
};
