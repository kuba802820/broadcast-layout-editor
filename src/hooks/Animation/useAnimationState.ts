import { ComponentState } from '@/components/ComponentStatePicker/config';
import { useLocalStorage } from '@rehooks/local-storage';

export const useAnimationState = () => {
    const [animState, setAnimState] = useLocalStorage<ComponentState>(
        'selectedAnimState'
    );
    const selectAnimState = (stateId: ComponentState) => setAnimState(stateId);
    return {
        animState,
        selectAnimState
    };
};
