import React, { createContext, useRef } from 'react';
import '../styles/global.scss';
import { AnimationSequence } from '@/components/AnimationSquence';
import { ComponentStatePicker } from '@/components/ComponentStatePicker';
import { LayoutPreview } from '@/components/LayoutPreview';
import { PropertyPanel } from '@/components/PropertyPanel';
import { ComponentCreator } from '@/components/ComponentCreator';
import { Toolbar } from '@/components/Toolbar';
import { useToolbox } from '@/hooks/Utils/useToolbox';
import { HierarchyComponents } from '@/components/HierarchyComponents';
import { useHandleHierarchy } from '@/hooks/Utils/useHandleHierarchy';
import { AnimationCreator } from '@/components/AnimationCreator';
import { usePreviewRenderer } from '@/hooks/Utils/usePreviewRenderer';
import { Component } from '@/schema/db/componentTypes';
import { useAnimation } from '@/hooks/Animation/useAnimation';

export const ComponentCtx = createContext<(Component[] | undefined)[]>([
    undefined
]);
const App = React.memo(() => {
    const { selectedTool } = useToolbox();
    const { previewRef, nodes } = useHandleHierarchy();
    const componentsRef = useRef<HTMLDivElement>(null);
    const animationRef = useRef<HTMLDivElement>(null);
    const componentsState = usePreviewRenderer(componentsRef);
    const { animState, animations } = useAnimation(animationRef);
    return (
        <div className="app">
            <div className="right-wrapper">
                <div className="toolbar-hierarchy">
                    <Toolbar />
                    <HierarchyComponents nodes={nodes} />
                </div>

                <div className="tools-wrapper">
                    <PropertyPanel />
                    <ComponentCreator selectedTool={selectedTool} />
                </div>
            </div>
            <div className="left-wrapper">
                <div id="component-animations" ref={animationRef} />
                <div id="components" ref={componentsRef} />

                <ComponentCtx.Provider value={componentsState}>
                    <div className="preview-wrapper" ref={previewRef}>
                        <LayoutPreview />
                    </div>
                </ComponentCtx.Provider>

                <div className="animation-tools">
                    <div>
                        <AnimationSequence
                            animations={animations}
                            animState={animState}
                        />

                        <AnimationCreator />
                    </div>
                    <ComponentStatePicker />
                </div>
            </div>
        </div>
    );
});

export default App;
