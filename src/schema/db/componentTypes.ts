export type ComponentType = 'box' | 'image' | 'text';
export type Component = {
    id: string;
    type: ComponentType;
    top: string;
    left: string;
    width: string;
    height: string;
    opacity: string;
    text?: string;
    backgroundColor?: string;
    imgSrc?: string;
    zindex?: string;
    isEditabled?: string;
    borderRadius?: string;
};
