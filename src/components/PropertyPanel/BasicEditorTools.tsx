import React, { useContext } from 'react';
import { HexColorPicker } from 'react-colorful';
import { ComponentsContext } from '.';

export const BasicEditorTools = () => {
    const { parsedValue, setValue } = useContext(ComponentsContext);
    return (
        <>
            {parsedValue ? (
                <div className="properties-wrapper">
                    <div className="properties">
                        {parsedValue && parsedValue.type === 'box' ? (
                            <div className={'selected-color-wrapper'}>
                                <div
                                    className="actual-selected-color"
                                    style={{
                                        backgroundColor:
                                            parsedValue.backgroundColor
                                    }}
                                ></div>
                                <p>Element background color</p>
                            </div>
                        ) : null}
                        {parsedValue && parsedValue.type === 'text' ? (
                            <div className="editable-text-wrapper">
                                <input
                                    type="checkbox"
                                    id="editable-text"
                                    checked={
                                        parsedValue.isEditabled === 'true'
                                            ? true
                                            : false
                                    }
                                    onChange={(e) => {
                                        setValue(
                                            JSON.stringify({
                                                ...parsedValue,
                                                isEditabled: `${e.target.checked}`
                                            })
                                        );
                                    }}
                                />
                                <label
                                    htmlFor="editable-text"
                                    className="editable-text-label"
                                >
                                    Editable text?
                                </label>
                            </div>
                        ) : null}
                    </div>
                    {parsedValue && parsedValue.type === 'box' ? (
                        <div className="color-picker-wrapper">
                            <HexColorPicker
                                color={parsedValue.backgroundColor}
                                onChange={(color) => {
                                    setValue(
                                        JSON.stringify({
                                            ...parsedValue,
                                            backgroundColor: color
                                        })
                                    );
                                }}
                                className={'color-picker'}
                            />
                        </div>
                    ) : null}
                </div>
            ) : null}
        </>
    );
};
