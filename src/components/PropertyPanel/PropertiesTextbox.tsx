import { useToolbox } from '@/hooks/Utils/useToolbox';
import React, { useContext } from 'react';
import { ComponentsContext } from '.';
import { Textbox } from '../UI/Textbox';

export const PropertiesTextbox = () => {
    const { updateComponent } = useToolbox();
    const { parsedValue, setValue } = useContext(ComponentsContext);
    const filteredPxUnit = (data: string) => data.replace(/px/g, '');

    return (
        <div className="editors">
            <div className="properties-text">
                {(() => {
                    if (parsedValue) {
                        const disabledTextboxes = ['id', 'type'];
                        const { text, isEditabled, ...rest } = parsedValue;
                        return Object.entries(rest).map(([name, val]) => (
                            <Textbox
                                label={name}
                                name={name}
                                key={name}
                                value={filteredPxUnit(val)}
                                disabled={
                                    disabledTextboxes.includes(name)
                                        ? true
                                        : false
                                }
                                onChange={(e) => {
                                    const { name, value } = e.target;
                                    setValue(
                                        JSON.stringify({
                                            ...parsedValue,
                                            [name]: value
                                        })
                                    );
                                    updateComponent(
                                        parsedValue.id,
                                        parsedValue
                                    );
                                }}
                            />
                        ));
                    } else {
                        return null;
                    }
                })()}
            </div>
        </div>
    );
};
