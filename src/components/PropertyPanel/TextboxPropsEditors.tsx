import { ContentState, convertToRaw, EditorState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import React, { useContext, useEffect, useState } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { ComponentsContext } from '.';
import { PropertiesTextbox } from './PropertiesTextbox';
import './style.scss';

export const TextboxPropsEditors = () => {
    const { setValue, parsedValue } = useContext(ComponentsContext);
    const [editorState, setEditorState] = useState<EditorState>();

    useEffect(() => {
        if (editorState) {
            setValue(
                JSON.stringify({
                    ...parsedValue,
                    text: draftToHtml(
                        convertToRaw(editorState.getCurrentContent())
                    )
                })
            );
        }
    }, [editorState, parsedValue, setValue]);

    useEffect(() => {
        if (parsedValue?.text && ContentState) {
            const blocksFromHtml = htmlToDraft(parsedValue.text);
            const { contentBlocks, entityMap } = blocksFromHtml;
            const contentState = ContentState.createFromBlockArray(
                contentBlocks,
                entityMap
            );
            setEditorState(EditorState.createWithContent(contentState));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <>
            <PropertiesTextbox />
            <div className="text-editor">
                {parsedValue &&
                parsedValue.type === 'text' &&
                parsedValue.isEditabled === 'false' ? (
                    <Editor
                        editorState={editorState}
                        toolbarClassName="toolbarClassName"
                        wrapperClassName="wrapperClassName"
                        editorClassName="editorClassName"
                        toolbar={{
                            options: [
                                'inline',
                                'blockType',
                                'fontSize',
                                'history',
                                'colorPicker',
                                'emoji',
                                'fontFamily'
                            ]
                        }}
                        onEditorStateChange={(e) => {
                            setEditorState(e);
                        }}
                    />
                ) : null}
            </div>
        </>
    );
};
