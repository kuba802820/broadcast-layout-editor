import { useHandleSelection } from '@/hooks/Utils/useHandleSelection';
import { useKeyboard } from '@/hooks/Utils/useKeyboard';
import { useToolbox } from '@/hooks/Utils/useToolbox';
import unselect from '@Assets/icons/unselect.svg';
import React, { useContext } from 'react';
import { ComponentsContext } from '.';
import { Button } from '../UI/Button';

export const PropertyButton = () => {
    const { deleteComponent } = useToolbox();
    const { clearSelection } = useHandleSelection();
    const { parsedValue } = useContext(ComponentsContext);

    useKeyboard('Escape', () => {
        clearSelection();
    });
    useKeyboard('Delete', () => {
        if (parsedValue) {
            const TEXTBOX_ID = 'textbox-field';
            const isFocused = document.activeElement?.id === TEXTBOX_ID;
            if (!isFocused) deleteComponent(parsedValue.id);
        }
    });
    return (
        <div className="buttons-properties">
            <Button
                icon={{
                    src: unselect,
                    height: '55',
                    width: '55'
                }}
                color={'red'}
                onClick={() => {
                    clearSelection();
                }}
            />
        </div>
    );
};
