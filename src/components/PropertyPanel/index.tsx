import React, { createContext, memo } from 'react';
import './style.scss';

import { Component } from '@/schema/db/componentTypes';
import { TextboxPropsEditors } from './TextboxPropsEditors';
import { BasicEditorTools } from './BasicEditorTools';
import { PropertyButton } from './PropertyButton';
import { useHandleSelection } from '@/hooks/Utils/useHandleSelection';
import { useUpdatePreview } from '@/hooks/Utils/useUpdatePreview';
import { NothingSelected } from '../NothingSelected';
interface Context {
    parsedValue: Component | undefined;
    setValue: (newValue: string) => void | undefined;
}
export const ComponentsContext = createContext<Context>({
    parsedValue: undefined,
    setValue: () => undefined
});

export const PropertyPanel = memo(() => {
    const { parsedValue, setValue } = useUpdatePreview();
    const { loading } = useHandleSelection();
    return (
        <div className={'property-panel'}>
            {!loading && parsedValue ? (
                <>
                    <ComponentsContext.Provider
                        value={{ parsedValue, setValue }}
                    >
                        <PropertyButton />
                        <TextboxPropsEditors />
                        <BasicEditorTools />
                    </ComponentsContext.Provider>
                </>
            ) : (
                <NothingSelected />
            )}
        </div>
    );
});
