import React, { FC, useState } from 'react';
import { Button } from '../UI/Button';
import './style.scss';
import tick from '@Assets/icons/tick.svg';
import { Textbox } from '../UI/Textbox';
import { useToolbox } from '@/hooks/Utils/useToolbox';
import { ComponentType } from '@/schema/db/componentTypes';
import { useHandleSelection } from '@/hooks/Utils/useHandleSelection';
import swal from 'sweetalert';
import { CREATE_COMPONENT_ERROR } from '@/config/notification';
type Props = {
    selectedTool: ComponentType;
};

export const ComponentCreator: FC<Props> = ({ selectedTool }) => {
    const [idElement, setIdElement] = useState('');
    const { clearSelection } = useHandleSelection();
    const { createComponent, getAllComponentsIds } = useToolbox();

    const defaultValue = {
        top: '0px',
        left: '0px',
        width: '100px',
        height: '100px',
        opacity: '1',
        backgroundColor: '#ff0000',
        borderRadius: '0px',
        ...(selectedTool === 'text' && {
            text: '<p>Example text</p>'
        }),
        ...(selectedTool === 'text' && {
            isEditabled: 'false'
        }),
        ...(selectedTool === 'image' && {
            imgSrc:
                'https://richardrosenman.com/wp-content/uploads/software_colorbars_image01.jpg'
        }),
        zindex: '1'
    };
    const validatedCreateComponent = () => {
        const firstCharIsLetter = idElement.match(/^[a-zA-Z].*/g);
        const componentsId = getAllComponentsIds();
        if (firstCharIsLetter && !componentsId?.includes(idElement)) {
            clearSelection();
            createComponent({
                id: idElement,
                type: selectedTool,
                ...defaultValue
            });
        } else {
            swal('Error!', CREATE_COMPONENT_ERROR, 'error');
        }
    };
    return (
        <div className={'component-creator'}>
            <Textbox
                label={'Id elementu: '}
                onChange={(e) => {
                    setIdElement(e.currentTarget.value);
                }}
            />
            <Button
                icon={{
                    src: tick,
                    height: '55',
                    width: '55'
                }}
                color={'aqua'}
                onClick={() => {
                    validatedCreateComponent();
                }}
            />
        </div>
    );
};
