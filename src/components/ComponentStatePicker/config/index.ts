import { ColorBtn, SizeBtn } from '@/components/UI/Button';
import disable from '@Assets/icons/disable.svg';
import enabled from '@Assets/icons/enabled.svg';
import update from '@Assets/icons/update.svg';

export type ComponentState = 'enabledstate' | 'disabledstate' | 'updatestate';
interface ComponentStatesConfig {
    icon: {
        src: string;
        width: string;
        height: string;
    };
    id?: ComponentState;
    size?: SizeBtn;
    color: ColorBtn;
}
export const componentStatesConfig: ComponentStatesConfig[] = [
    {
        icon: {
            src: enabled,
            width: '100px',
            height: '100px'
        },
        color: 'green',
        size: 'lg',
        id: 'enabledstate'
    },
    {
        icon: {
            src: disable,
            width: '100px',
            height: '100px'
        },
        color: 'red',
        size: 'lg',
        id: 'disabledstate'
    },
    {
        icon: {
            src: update,
            width: '80px',
            height: '80px'
        },
        color: 'light-blue',
        size: 'lg',
        id: 'updatestate'
    }
];
