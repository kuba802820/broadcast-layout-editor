import { useAnimationState } from '@/hooks/Animation/useAnimationState';
import React from 'react';
import { Button } from '../UI/Button';
import { ComponentState, componentStatesConfig } from './config';
import './style.scss';

export const ComponentStatePicker = () => {
    const { animState, selectAnimState } = useAnimationState();
    return (
        <div className={'component-state-picker'}>
            <div className={'selected-tool'}>
                Selected anim state: {animState}
            </div>
            {componentStatesConfig.map(({ icon, color, size, id }) => {
                return (
                    <Button
                        key={id}
                        icon={{
                            src: icon.src,
                            height: icon.height,
                            width: icon.width
                        }}
                        id={id}
                        size={size}
                        color={color}
                        onClick={({ currentTarget }) => {
                            const componentState = currentTarget.id as ComponentState;
                            selectAnimState(componentState);
                        }}
                    />
                );
            })}
        </div>
    );
};
