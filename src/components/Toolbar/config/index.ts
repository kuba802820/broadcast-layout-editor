import { ButtonsProps } from '@/components/UI/Button';
import box from '@Assets/icons/box.svg';
import image from '@Assets/icons/image.svg';
import text from '@Assets/icons/text.svg';

export const ButtonsConfig: Omit<ButtonsProps, 'onClick'>[] = [
    {
        icon: {
            src: box,
            width: '65px',
            height: '65px'
        },
        color: 'light-blue',
        size: 'lg',
        id: 'box',
        addSpace: true
    },
    {
        icon: {
            src: image,
            width: '65px',
            height: '65px'
        },
        color: 'light-blue',
        size: 'lg',
        id: 'image',
        addSpace: true
    },
    {
        icon: {
            src: text,
            width: '65px',
            height: '65px'
        },
        color: 'light-blue',
        size: 'lg',
        id: 'text'
    }
];
