import { useToolbox } from '@/hooks/Utils/useToolbox';
import { ComponentType } from '@/schema/db/componentTypes';
import React from 'react';
import { Button } from '../UI/Button';
import { ButtonsConfig } from './config';
import './style.scss';
export const Toolbar = () => {
    const { selectTool, selectedTool } = useToolbox();
    return (
        <div className={'toolbar-wrapper'}>
            <div className={'selected-tool'}>Selected tool: {selectedTool}</div>
            {ButtonsConfig.map(({ color, icon, size, id, addSpace }) => {
                const { src, height, width } = icon;
                return (
                    <Button
                        icon={{
                            src,
                            height,
                            width
                        }}
                        key={id}
                        color={color}
                        size={size}
                        addSpace={addSpace}
                        id={id}
                        onClick={() => {
                            const ComponentId = id as ComponentType;
                            if (id) selectTool(ComponentId);
                        }}
                    />
                );
            })}
        </div>
    );
};
