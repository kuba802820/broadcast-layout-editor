import clsx from 'clsx';
import React, { FC, MouseEvent } from 'react';
import SVG from 'react-inlinesvg';
import '../styles/btn.scss';

export type ColorBtn = 'light-blue' | 'aqua' | 'green' | 'light-aqua' | 'red';
export type SizeBtn = 'sm' | 'lg';
export interface ButtonsProps {
    icon: {
        src: string;
        width: string;
        height: string;
    };
    id?: string;
    size?: SizeBtn;
    addSpace?: boolean;
    color: ColorBtn;
    smallRectBtn?: boolean;
    onClick: (e: MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => void;
}

export const Button: FC<ButtonsProps> = ({
    icon,
    color,
    size,
    onClick,
    id,
    smallRectBtn = false,
    addSpace = false
}) => {
    const { width, height, src } = icon;
    const btnSize = (size: SizeBtn | undefined) =>
        size === 'lg' ? 'lg-btn' : smallRectBtn ? 'square-btn' : 'sm-btn';

    return (
        <button
            className={clsx(
                `${color}`,
                'button',
                addSpace ? 'space-btn' : '',
                btnSize(size)
            )}
            onClick={onClick}
            id={id}
        >
            <SVG src={src} width={width} height={height} />
        </button>
    );
};
