import React, { FC } from 'react';
import '../styles/textbox.scss';

type Props = {
    label?: string;
    placeholder?: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    value?: string | number;
    name?: string;
    disabled?: boolean;
};

export const Textbox: FC<Props> = ({
    placeholder,
    label,
    name,
    value,
    disabled,
    onChange
}) => {
    return (
        <>
            <div className="textbox-wrapper">
                {label ? (
                    <label htmlFor="textbox-field" className="textbox-label">
                        {label}
                    </label>
                ) : null}
                <input
                    id={'textbox-field'}
                    name={name}
                    placeholder={placeholder}
                    value={value}
                    disabled={disabled}
                    onChange={onChange}
                />
            </div>
        </>
    );
};
