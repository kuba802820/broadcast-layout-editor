import { useHandleSelection } from '@/hooks/Utils/useHandleSelection';
import { Component } from '@/schema/db/componentTypes';
import React, { FC } from 'react';
type Props = {
    htmlNode?: HTMLElement;
};

export const HierarchyItem: FC<Props> = ({ htmlNode }) => {
    const { selectComponent, getDomElement } = useHandleSelection();

    const selectItem = (htmlNode: HTMLElement) => {
        const componentData = getDomElement('components');
        if (componentData) {
            const data = componentData.dataset[htmlNode.id];
            if (data) {
                const ComponentValue: Component = JSON.parse(data);
                selectComponent(ComponentValue, htmlNode);
            }
        }
    };

    return (
        <>
            {htmlNode && htmlNode.id ? (
                // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                <div
                    className={'hierarchy-item'}
                    role="button"
                    tabIndex={0}
                    onClick={() => {
                        selectItem(htmlNode);
                    }}
                >
                    {htmlNode.id}
                </div>
            ) : null}
        </>
    );
};
