import React, { FC } from 'react';
import { HierarchyItem } from './HierarchyItem';
import './style.scss';
type Props = {
    nodes: NodeList[] | undefined;
};
export const HierarchyComponents: FC<Props> = ({ nodes }) => {
    return (
        <div className={'hierarchy-components-wrapper'}>
            {nodes && nodes?.length > 0
                ? nodes?.map((val) => {
                      if (val.length > 0) {
                          const el = val[0] as HTMLElement;
                          if (el.id) {
                              return (
                                  <HierarchyItem key={el.id} htmlNode={el} />
                              );
                          }
                      }
                  })
                : null}
        </div>
    );
};
