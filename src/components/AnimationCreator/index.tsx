import { CREATE_ANIMATION_ERROR } from '@/config/notification';
import { useManageAnimation } from '@/hooks/Animation/useManageAnimation';
import { useHandleSelection } from '@/hooks/Utils/useHandleSelection';
import insert_text_change from '@Assets/icons/insert_text_change.svg';
import tick from '@Assets/icons/tick.svg';
import useLocalStorage from '@rehooks/local-storage';
import React, { useState } from 'react';
import swal from 'sweetalert';
import { ComponentState } from '../ComponentStatePicker/config';
import { Button } from '../UI/Button';
import { Textbox } from '../UI/Textbox';
import './style.scss';

export const AnimationCreator = () => {
    const { saveSequenceItem } = useManageAnimation();
    const [animName, setAnimName] = useState('');
    const [animState] = useLocalStorage<ComponentState>('selectedAnimState');
    const { getDomElement, parsedComponent } = useHandleSelection();
    const isEditabledText =
        parsedComponent?.isEditabled === 'true' ? true : false;

    const insertIntoSequence = (
        callback: (animState: ComponentState, componentData: string) => void
    ) => {
        const components = getDomElement('components');
        const selectedComponent = getDomElement();
        if (selectedComponent) {
            const componentData = components?.dataset[selectedComponent?.id];
            if (animState && componentData) {
                callback(animState, componentData);
            }
        } else {
            swal('Error!', CREATE_ANIMATION_ERROR, 'error');
        }
    };

    return (
        <div className={'animation-creator'}>
            <Textbox
                label={'Nazwa animacji: '}
                onChange={(e) => {
                    setAnimName(e.currentTarget.value);
                }}
            />
            <div className="animation-creator-button">
                <Button
                    icon={{
                        src: tick,
                        height: '55',
                        width: '55'
                    }}
                    size={'sm'}
                    color={'aqua'}
                    onClick={() => {
                        insertIntoSequence(
                            (
                                animState: ComponentState,
                                componentData: string
                            ) => {
                                saveSequenceItem(animState, {
                                    name: animName,
                                    delay: 300,
                                    duration: 300,
                                    seq: JSON.parse(componentData)
                                });
                            }
                        );
                    }}
                />
                {isEditabledText ? (
                    <Button
                        icon={{
                            src: insert_text_change,
                            height: '55',
                            width: '55'
                        }}
                        size={'sm'}
                        color={'light-blue'}
                        onClick={() => {
                            insertIntoSequence((animState: ComponentState) => {
                                saveSequenceItem(animState, {
                                    name: animName,
                                    isChangeText: true,
                                    delay: 300
                                });
                            });
                        }}
                    />
                ) : null}
            </div>
        </div>
    );
};
