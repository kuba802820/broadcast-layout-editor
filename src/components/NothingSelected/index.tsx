import React from 'react';
import Loader from 'react-loader-spinner';
import '../../styles/global.scss';
export const NothingSelected = () => {
    return (
        <div className="loader">
            <Loader type="ThreeDots" color="#00BFFF" height={100} width={100} />
            <p>Nothing selected</p>
        </div>
    );
};
