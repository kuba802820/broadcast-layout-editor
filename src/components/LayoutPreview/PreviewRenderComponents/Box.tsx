import { createElement } from 'react';
import { PropsPreviewComponent } from '../PreviewComponents';

export function Box<T>({
    commonCssProperties,
    component,
    selectComponent
}: PropsPreviewComponent<T>) {
    return createElement('div', {
        key: component.id,
        style: {
            ...commonCssProperties,
            backgroundColor: `${component.backgroundColor}`
        },
        className: `item-preview`,
        onClick: ({ currentTarget }) => {
            selectComponent(component, currentTarget);
        },
        id: component.id
    });
}
