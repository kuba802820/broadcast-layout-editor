import { createElement } from 'react';
import { PropsPreviewComponent } from '../PreviewComponents';

export function Text<T>({
    commonCssProperties,
    component,
    selectComponent
}: PropsPreviewComponent<T>) {
    return createElement('div', {
        key: component.id,
        style: {
            ...commonCssProperties,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        dangerouslySetInnerHTML: {
            __html: `${component.text}`
        },
        className: `item-preview`,
        onClick: ({ currentTarget }) => {
            selectComponent(component, currentTarget);
        },
        id: component.id
    });
}
