import { createElement } from 'react';
import { PropsPreviewComponent } from '../PreviewComponents';

export function Image<T>({
    commonCssProperties,
    component,
    selectComponent
}: PropsPreviewComponent<T>) {
    console.log(component);
    return createElement('img', {
        key: component.id,
        style: {
            ...commonCssProperties
        },
        className: `item-preview`,
        src: component.imgSrc,
        onClick: ({ currentTarget }) => {
            selectComponent(component, currentTarget);
        },
        id: component.id
    });
}
