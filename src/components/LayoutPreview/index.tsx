import { useHandleSelection } from '@/hooks/Utils/useHandleSelection';
import React, { useEffect } from 'react';
import { PreviewComponents } from './PreviewComponents';
import './style.scss';
export const LayoutPreview = React.memo(() => {
    const { clearSelection } = useHandleSelection();
    useEffect(() => {
        clearSelection();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <PreviewComponents />
        </>
    );
});
