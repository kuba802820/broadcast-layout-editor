import { ComponentCtx } from '@/app/App';
import { useHandleSelection } from '@/hooks/Utils/useHandleSelection';
import { Component } from '@/schema/db/componentTypes';
import React, { useContext } from 'react';
import { Box } from './PreviewRenderComponents/Box';
import { Image } from './PreviewRenderComponents/Image';
import { Text } from './PreviewRenderComponents/Text';

export interface PropsPreviewComponent<T> {
    commonCssProperties: T;
    component: Component;
    selectComponent: (
        item: Component,
        currentTarget: EventTarget & Element
    ) => void;
}

export const PreviewComponents = React.memo(() => {
    const { selectComponent } = useHandleSelection();
    const [componentsState] = useContext(ComponentCtx);

    return (
        <>
            {(() => {
                if (componentsState) {
                    return componentsState?.map((component) => {
                        const commonCssProperties = {
                            position: 'absolute',
                            top: `${component.top}`,
                            left: `${component.left}`,
                            width: `${component.width}`,
                            height: `${component.height}`,
                            opacity: `${component.opacity}`,
                            zIndex: `${component.zindex}`,
                            borderRadius: `${component.borderRadius}`
                        };
                        switch (component.type) {
                            case 'box':
                                return Box<typeof commonCssProperties>({
                                    commonCssProperties,
                                    component,
                                    selectComponent
                                });

                            case 'text':
                                return Text<typeof commonCssProperties>({
                                    commonCssProperties,
                                    component,
                                    selectComponent
                                });

                            case 'image':
                                return Image<typeof commonCssProperties>({
                                    commonCssProperties,
                                    component,
                                    selectComponent
                                });

                            default:
                                break;
                        }
                    });
                } else {
                    return null;
                }
            })()}
        </>
    );
});
