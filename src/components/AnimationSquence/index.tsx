import { usePlayAnimation } from '@/hooks/Animation/usePlayAnimation';
import { Component } from '@/schema/db/componentTypes';
import play_anim from '@Assets/icons/play-anim.svg';
import React, { FC } from 'react';
import { ComponentState } from '../ComponentStatePicker/config';
import { NothingSelected } from '../NothingSelected';
import { Button } from '../UI/Button';
import { AnimationItem } from './AnimationItem';
import './style.scss';

export type AnimationObject = {
    name: string;
    isChangeText?: boolean;
    delay: number;
    duration?: number;
    seq?: Component;
};

type Props = {
    animations: AnimationObject[] | null;
    animState: ComponentState | null;
};

export const AnimationSequence: FC<Props> = ({ animations, animState }) => {
    const { playAnimSeq } = usePlayAnimation();
    return (
        <div className={'animation-sequence'}>
            {animations && animations.length > 0 ? (
                <div>
                    <div className="animation-sequence-list">
                        {animations.map((animation, idx, arr) => {
                            if (animState) {
                                return (
                                    <AnimationItem
                                        animation={animation}
                                        index={idx}
                                        animState={animState}
                                        animArray={arr}
                                    />
                                );
                            }
                        })}
                    </div>
                    <Button
                        icon={{
                            src: play_anim,
                            height: '55',
                            width: '55'
                        }}
                        color={'green'}
                        onClick={() => {
                            playAnimSeq(animations);
                        }}
                    />
                </div>
            ) : (
                <NothingSelected />
            )}
        </div>
    );
};
