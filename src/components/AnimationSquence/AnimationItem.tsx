import { useManageAnimation } from '@/hooks/Animation/useManageAnimation';
import arrow_down from '@Assets/icons/arrow-down.svg';
import arrow_up from '@Assets/icons/arrow-up.svg';
import React, { FC } from 'react';
import { AnimationObject } from '.';
import { ComponentState } from '../ComponentStatePicker/config';
import { Button } from '../UI/Button';
import { AnimationManageBtn } from './AnimationManageBtn';
import { AnimationTextBoxes } from './AnimationsTextBoxes';
import './style.scss';

export type AnimProps = {
    animation: AnimationObject;
    index: number;
    animState: ComponentState;
    animArray: AnimationObject[];
};

export const AnimationItem: FC<AnimProps> = ({
    animation,
    index,
    animState,
    animArray
}) => {
    const { moveAnimation } = useManageAnimation();
    return (
        <>
            <div className="animation-item">
                {index > 0 ? (
                    <Button
                        icon={{
                            src: arrow_up,
                            height: '31',
                            width: '31'
                        }}
                        smallRectBtn={true}
                        color={'light-aqua'}
                        onClick={() => {
                            moveAnimation(index, animState, 'up');
                        }}
                    />
                ) : null}
                {index !== animArray.length - 1 ? (
                    <Button
                        icon={{
                            src: arrow_down,
                            height: '31',
                            width: '31'
                        }}
                        color={'green'}
                        smallRectBtn={true}
                        onClick={() => {
                            moveAnimation(index, animState, 'down');
                        }}
                    />
                ) : null}

                <AnimationTextBoxes animation={animation} />
                <AnimationManageBtn
                    animation={animation}
                    index={index}
                    animState={animState}
                />
            </div>
        </>
    );
};
