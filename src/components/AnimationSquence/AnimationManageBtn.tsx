import { useManageAnimation } from '@/hooks/Animation/useManageAnimation';
import { usePlayAnimation } from '@/hooks/Animation/usePlayAnimation';
import { useParsedLocalStorage } from '@/hooks/Utils/useParsedLocalStorage';
import play_anim from '@Assets/icons/play-anim.svg';
import remove from '@Assets/icons/remove.svg';
import show_anim_pos from '@Assets/icons/show-anim-pos.svg';
import React, { Component, FC } from 'react';
import { Button } from '../UI/Button';
import { AnimProps } from './AnimationItem';
export const AnimationManageBtn: FC<
    Pick<AnimProps, 'animation' | 'index' | 'animState'>
> = ({ animation, index, animState }) => {
    const { playAnimation } = usePlayAnimation();
    const { removeOneAnimation } = useManageAnimation();
    const { setValue } = useParsedLocalStorage<Component>('selectionComponent');
    return (
        <>
            {!animation.isChangeText ? (
                <>
                    <Button
                        icon={{
                            src: play_anim,
                            height: '31',
                            width: '31'
                        }}
                        color={'green'}
                        smallRectBtn={true}
                        onClick={() => {
                            playAnimation({ animation });
                        }}
                    />
                    <Button
                        icon={{
                            src: show_anim_pos,
                            height: '31',
                            width: '31'
                        }}
                        color={'light-blue'}
                        smallRectBtn={true}
                        onClick={() => {
                            setValue(JSON.stringify(animation.seq));
                        }}
                    />
                </>
            ) : null}

            <Button
                icon={{
                    src: remove,
                    height: '31',
                    width: '31'
                }}
                color={'red'}
                smallRectBtn={true}
                onClick={() => {
                    removeOneAnimation(index, animState);
                }}
            />
        </>
    );
};
