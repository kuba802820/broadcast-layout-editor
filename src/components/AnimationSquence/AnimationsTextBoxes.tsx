import React, { FC } from 'react';
import { AnimationObject } from '.';
import { Textbox } from '../UI/Textbox';

type Props = {
    animation: AnimationObject;
};

export const AnimationTextBoxes: FC<Props> = ({ animation }) => {
    return (
        <>
            <Textbox
                label={'Name'}
                name={'asdassa'}
                value={animation.name}
                onChange={(e) => {}}
            />
            {!animation.isChangeText ? (
                <Textbox
                    label={'Duration'}
                    name={'asdassa'}
                    value={animation.duration}
                    onChange={(e) => {}}
                />
            ) : null}

            <Textbox
                label={'Delay'}
                name={'asdassa'}
                value={animation.delay}
                onChange={(e) => {}}
            />
        </>
    );
};
