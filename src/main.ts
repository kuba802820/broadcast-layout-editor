import { app, BrowserWindow } from 'electron';
import isDev from 'electron-is-dev';
const createWindow = (): void => {
    const win = new BrowserWindow({
        width: 1980,
        height: 1080,
        webPreferences: {
            nodeIntegration: true
        }
    });
    win.setMenuBarVisibility(isDev);
    win.loadURL(
        isDev
            ? 'http://localhost:9000'
            : `file://${app.getAppPath()}/index.html`
    );
};

app.on('ready', createWindow);
