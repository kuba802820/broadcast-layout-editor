export const CREATE_COMPONENT_ERROR =
    'Please make sure that the name of element does start from small or great letter, and the id element does not repeat itself!';

export const CREATE_ANIMATION_ERROR =
    'You must select any component before you create animation';
